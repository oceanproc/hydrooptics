clear all
close all
clc
% srf - spectral response function (relative untits)
% https://oceancolor.gsfc.nasa.gov/docs/rsr/rsr_tables/
% https://sentinel.esa.int/web/sentinel/technical-guides/sentinel-3-olci/olci-instrument/spectral-response-function-data
% https://ncc.nesdis.noaa.gov/NOAA-20/J1VIIRS_NOAA20_SpectralResponseFunctions.php
% https://suzaku.eorc.jaxa.jp/GCOM_C/data/prelaunch/index.html

id = 'MODIS-TERRA'; % terra
id = 'MODIS-AQUA'; % aqua
% id = 'VIIRS-SNPP'; %
% id = 'VIIRS-JPSS1';
% id = 'OLCI-S3A';
% id = 'OLCI-S3B';
% id = 'SGLI-GCOMC';

wave_lim = [325 1075];
band_lim = [325 900];
toplot = 0;

fpath_m = mfilename('fullpath');
dpath_m = fileparts(fpath_m);

dpath_srf = [dpath_m,'\source\'];
dpath_save = [dpath_m,'\data\'];
dpath_img  = [dpath_save,'img\'];
if ~isdir(dpath_save); mkdir(dpath_save); end
if ~isdir(dpath_img); mkdir(dpath_img); end

switch id
    case 'MODIS-AQUA'
        fname_srf = 'HMODISA_RSRs.txt';
    case 'MODIS-TERRA'
        fname_srf = 'HMODIST_RSRs.txt';
    case 'VIIRS-SNPP'
        fname_srf = 'VIIRSN_IDPSv3_RSRs.txt';
    case 'VIIRS-JPSS1'
        dpath_add = 'J1_VIIRS_RSR_DAWG_At-Launch_Public_Release_V2.1_Nov2016\J1_VIIRS_BA_RSR_V2F\';
        fname_mask = 'J1_VIIRS_RSR_M*_BA_Fused_V2F.txt';
    case 'OLCI-S3A'
        fname_srf = 'S3A_OL_SRF_20160713_mean_rsr.nc4';
    case 'OLCI-S3B'
        fname_srf = 'S3B_OL_SRF_0_20180109_mean_rsr.nc4';
    case 'SGLI-GCOMC'
        fname_srf = 'sgli_rsr_f_N_PFM201607.txt';
%         fname_srf = 'sgli_rsr_l_N_PFM201607.txt';
%         fname_srf = 'sgli_rsr_f_for_algorithm_PFM201607.txt';
end
fname_save = ['srf_',id,'.mat'];
fname_img  = ['srf_',id,'.jpg'];

data_wave = {};
data = {};
switch id
    case {'MODIS-AQUA','MODIS-TERRA','VIIRS-SNPP'}
        fpath = [dpath_srf,fname_srf];
        fp = fopen(fpath);
        data_file = textscan(fp,'%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f','headerlines',8);
        data = data_file(2:end);
        data_wave = repmat(data_file(1),[1 length(data)]);
        fclose(fp);
    case 'VIIRS-JPSS1'
        files = dir([dpath_srf,dpath_add,fname_mask]);
        n = 0;
        for i = 1:length(files)
            fpath = [dpath_srf,dpath_add,files(i).name];
            fp = fopen(fpath);
            data_file = textscan(fp,'%*s %f %f','headerlines',4);
            fclose(fp);
            n = n + 1;
            data_wave(n) = data_file(1);
            data(n) = data_file(2);
        end
    case {'OLCI-S3A','OLCI-S3B'}
        fpath = [dpath_srf,fname_srf];
        %         ncdisp(fpath)
        data_file1 = ncread(fpath,'mean_spectral_response_function');
        data_file2 = ncread(fpath,'mean_spectral_response_function_wavelength');
        
        for i = 1:size(data_file1,2)
            data(i) = {data_file1(:,i)};
            data_wave(i) = {data_file2(:,i)};
        end
    case 'SGLI-GCOMC'
        fpath = [dpath_srf,fname_srf];
        fp = fopen(fpath);
        data_file = textscan(fp,'%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f','headerlines',1);
        fclose(fp);
        data      = data_file(2:2:end);
        data_wave = data_file(1:2:end);
        for i = 1:length(data)
            data{i} = data{i}/max(data{i});
        end
end

OUT.wave0 = [];
OUT.band_fun = {};
OUT.band_name = {};
OUT.wave_lim = wave_lim;
n = 0;
for i = 1:length(data)
    
    wave = data_wave{i};
%     wave = wave(1:3:end);
    ok_wave = wave >= wave_lim(1) & wave <= wave_lim(2);
    wave_   = wave(ok_wave);
    wave_left  = (wave_lim(1):1:min(wave_)-1)';
    wave_right = (max(wave_)+1:wave_lim(2))';
    wave_ = [wave_left;wave_;wave_right];
    
    srf = data{i};
%     srf = srf(1:3:end);
    srf = data{i}/max(srf);
    srf   = srf(ok_wave);
    srf(srf<0) = 0;
    srf = [zeros(size(wave_left));srf;zeros(size(wave_right))];
    
    ok50   = srf > .5;
    wave50 = wave_(ok50);
    srf50  = srf(ok50);
    
    [~,ima] = max(srf50);
    wave_ma = wave50(ima);
    if isempty(wave_ma) || wave_ma < band_lim(1) || wave_ma > band_lim(2)
        continue
    end
    
    wave_mean  = mean(wave_(srf>.5));
    wave_wmean = sum(srf50.*wave50)./sum(srf50);
    
    %     f = fit(wave_,srf,'SmoothingSpline','SmoothingParam',1);
    f = griddedInterpolant(wave_,srf,'linear');
    wavei = wave_lim(1):.01:wave_lim(2);
    idx_left  = find(f(wavei)>.5,1,'first');
    idx_right = find(f(wavei)>.5,1,'last');
    wave_border1 = fminbnd(@(x)abs(f(x)-0.5),wavei(idx_left-2),wavei(idx_left+1));
    wave_border2 = fminbnd(@(x)abs(f(x)-0.5),wavei(idx_right-1),wavei(idx_right+2));
    wave_center = .5*(wave_border1+wave_border2);
    wave_width = wave_border2 - wave_border1;
    
    if toplot
        figure, hold on
        wave_plot = min(wave_):.1:max(wave_);
        plot(wave_,srf,'k.-')
        plot(wave_plot,f(wave_plot),'g-')
        grid on
        title(sprintf('wave-ma = %.2f, wave-mean = %.2f, wave-wmean = %.2f',wave_ma,wave_mean,wave_wmean))
        xlabel('\lambda, nm')
        ylabel('rsr')
    end
    
    n = n + 1;
    OUT.wave0(n,1) = wave_wmean;
    OUT.wave_peak(n,1) = wave_ma;
    OUT.wave_center(n,1) = roundn(wave_center,-3);
    OUT.wave_width(n,1) = roundn(wave_width,-3);
    OUT.band_fun{n,1} = f;
    OUT.band_name{n,1} = sprintf('band%d',i);
    
end
OUT.wave = (wave_lim(1):.1:wave_lim(2))';

% close all
figure('PaperUnits','centimeters','PaperPosition',[0 0 30 14]), hold on
color = jet(length(OUT.wave0));
plot_lim = [400 430];
for i = 1:length(OUT.wave0)
    if OUT.wave0(i) > plot_lim(2), continue, end
    if OUT.wave0(i) < plot_lim(1), continue, end
    plot(OUT.wave,OUT.band_fun{i}(OUT.wave),'k-','color',color(i,:))
end

grid on
set(gca,'ylim',[0 1])
xlabel('\lambda, nm')
ylabel('rsr')
title(id)
print('-r250','-djpeg',[dpath_img,fname_img])
save([dpath_save,fname_save],'-struct','OUT')

for i = 1:length(OUT.wave_center)
    if OUT.wave_center(i) > 600, break, end
    fprintf('\t%.2f/%.2f',roundn(OUT.wave_center(i),-2),roundn(OUT.wave_width(i),-2))
end
fprintf('\n%d\n',i-1)