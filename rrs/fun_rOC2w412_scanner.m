function [r,rrs_blue,rrs_norm] = funBioOptic_rOC2w412_scanner(lambda,rrs,method)

lambda = lambda(:);

switch method
    case 'MODIS-AQUA-v1'
        band_blue = 1;
        band_norm = 6;
        id = 'modis_aqua_v1';
    case 'MODIS-AQUA-v2'
        band_blue = 1;
        band_norm = 7;
        id = 'modis_aqua_v2';
    case 'MODIS-TERRA-v1'
        band_blue = 1;
        band_norm = 6;
        id = 'modis_terra_v1';
    case 'MODIS-TERRA-v2'
        band_blue = 1;
        band_norm = 7;
        id = 'modis_terra_v2';
    case 'OLCI-S3A'
        band_blue = 2;
        band_norm = 6;
        id = 'olci_s3a';
    case 'OLCI-S3B'
        band_blue = 2;
        band_norm = 6;
        id = 'olci_s3b';
    case 'VIIRS-SNPP'
        band_blue = 1;
        band_norm = 4;
        id = 'viirs_snpp';
    case 'VIIRS-JPSS1'
        band_blue = 1;
        band_norm = 4;
        id = 'viirs_jpss1';
    case 'SGLI-GCOMC'
        band_blue = 2;
        band_norm = 6;
        id = 'sgli_gcomc';
end

pos = find(method == '-');
if length(pos) > 1
    scanner = method(1:pos(2)-1);
else
    scanner = method;
end

fpath_m = mfilename('fullpath');
dpath_srf = [fileparts(fpath_m),'\scanners_srf\data\'];
fpath_srf = [dpath_srf,'srf_',scanner,'.mat'];
srf = load(fpath_srf);

weight_blue = srf.band_fun{band_blue}(lambda);
weight_norm = srf.band_fun{band_norm}(lambda);

rrs_blue = nan(size(rrs,2),1);
rrs_norm = rrs_blue;
for i = 1:size(rrs,2)
    
    rrs_ = rrs(:,i);
    
    rrs_blue(i,1) = sum(rrs_.*weight_blue)/sum(weight_blue);
    rrs_norm(i,1) = sum(rrs_.*weight_norm)/sum(weight_norm);

end

r = rrs_blue./rrs_norm;
r = log10(r(:));