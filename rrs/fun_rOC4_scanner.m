function [r,rrs_blue1,rrs_blue2,rrs_blue3,rrs_norm] = funBioOptic_rOC4_scanner(lambda,rrs,method)
% https://oceancolor.gsfc.nasa.gov/atbd/chlor_a/
% https://www.researchgate.net/publication/285869893_Ocean_chlorophyll_a_algorithms_for_Sea_WiFS_OC2_and_OC4_Version_4
lambda = lambda(:);

switch method
    case 'MODIS-AQUA-v1'
        band_blue1 = 3;
        band_blue2 = 4;
        band_blue3 = 5;
        band_norm = 6;
        id = 'modis_aqua_v1';
    case 'MODIS-AQUA-v2'
        band_blue1 = 3;
        band_blue2 = 4;
        band_blue3 = 5;
        band_norm = 7;
        id = 'modis_aqua_v2';
    case 'MODIS-TERRA-v1'
        band_blue1 = 3;
        band_blue2 = 4;
        band_blue3 = 5;
        band_norm = 6;
        id = 'modis_terra_v1';
    case 'MODIS-TERRA-v2'
        band_blue1 = 3;
        band_blue2 = 4;
        band_blue3 = 5;
        band_norm = 7;
        id = 'modis_terra_v2';
    case 'OLCI-S3A'
        band_blue1 = 3;
        band_blue2 = 4;
        band_blue3 = 5;
        band_norm = 6;
        id = 'olci_s3a';
    case 'OLCI-S3B'
        band_blue1 = 3;
        band_blue2 = 4;
        band_blue3 = 5;
        band_norm = 6;
        id = 'olci_s3b';
    case 'SGLI-GCOMC'
        band_blue1 = 3;
        band_blue2 = 4;
        band_blue3 = 5;
        band_norm = 6;
        id = 'sgli_gcomc';
end

pos = find(method == '-');
if length(pos) > 1
    scanner = method(1:pos(2)-1);
else
    scanner = method;
end

fpath_m = mfilename('fullpath');
dpath_srf = [fileparts(fpath_m),'\scanners_srf\data\'];
fpath_srf = [dpath_srf,'srf_',scanner,'.mat'];
srf = load(fpath_srf);

weight_blue1 = srf.band_fun{band_blue1}(lambda);
weight_blue2 = srf.band_fun{band_blue2}(lambda);
weight_blue3 = srf.band_fun{band_blue3}(lambda);
weight_norm = srf.band_fun{band_norm}(lambda);

% figure, hold on
% plot(weight_blue1)
% plot(weight_blue2)
% plot(weight_blue3)
% plot(weight_norm)

rrs_blue1 = nan(size(rrs,2),1);
rrs_blue2 = rrs_blue1;
rrs_blue3 = rrs_blue1;
rrs_norm = rrs_blue1;
for i = 1:size(rrs,2)
    
    rrs_ = rrs(:,i);
    
    rrs_blue1(i,1) = sum(rrs_.*weight_blue1)/sum(weight_blue1);
    rrs_blue2(i,1) = sum(rrs_.*weight_blue2)/sum(weight_blue2);
    rrs_blue3(i,1) = sum(rrs_.*weight_blue3)/sum(weight_blue3);
    rrs_norm(i,1) = sum(rrs_.*weight_norm)/sum(weight_norm);

end

r = max([rrs_blue1,rrs_blue2,rrs_blue3],[],2)./rrs_norm;
r = log10(r(:));