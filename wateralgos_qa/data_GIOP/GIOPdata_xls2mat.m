clear all
close all
clc

p = mfilename('fullpath');
pos = find(p == '/' | p == '\');
DIR_save = p(1:pos(end-1));

num = xlsread('GIOP.xlsx','aph','A4:C154');
W = num(:,1);
w = (min(W):.01:max(W))';
DAphi = num(:,2);
DEphi = num(:,3);

% f = fit(W,Aphi,'SmoothingSpline','SmoothingParam',.9);
Aphi = fit(W,DAphi,'cubicspline');
% Ephi = fit(W,DEphi,'cubicspline');
Ephi = fit(W,DEphi,'SmoothingSpline','SmoothingParam',.1);
aphstar = @(w,chl)Aphi(w).*chl.^(Ephi(w)-1);
%aph_ = 0.055/aphstar(443,chl0)*aphstar(w,chl0);

aph_ = @(w,chl0)0.055*(Aphi(w)/Aphi(443)).*chl0.^(Ephi(w)-Ephi(443));

% ������ GIOP plus v1
% aph_ = @(w,chl0)0.055*(Aphi(w)/Aphi(443)).*chl0.^(Ephi(w)-Ephi(443)+1); %


figure, hold on
plot(W,DAphi,'k.')
plot(w,Aphi(w),'g-')

figure, hold on
plot(W,DEphi,'k.')
plot(w,Ephi(w),'g-')

figure, hold on
plot(W,aph_(W,.5),'k.')
plot(w,aph_(w,.5),'g-')

num = xlsread('GIOP.xlsx','aw','A3:B142');
W = num(:,1);
w = min(W):.01:max(W);
Daw = num(:,2);

% aw = fit(W,Daw,'cubicspline');
aw = fit(W,Daw,'SmoothingSpline','SmoothingParam',.2);

figure, hold on
plot(W,Daw,'k.')
plot(w,aw(w),'g-')

adg_ = @(w,w0,Sg)exp(-Sg*(w-w0));
figure, hold on
Sg = 0.02061;
w0 = 443;
plot(w,adg_(w,w0,Sg),'g-','LineWidth',2)
plot(w,adg_(w,w0,0.018),'r--')
grid on

% a = @(w,Mph,Mdg,chl0,w0,Sg)aw(w) + Mph*aph_(w,chl0) + Mdg*adg_(w,w0,Sg);

% ������ GIOP plus - v2
a = @(w,Mph,Mdg,chl0,w0,Sg)aw(w) + Mph*chl0*aph_(w,chl0) + Mdg*adg_(w,w0,Sg);

figure, hold on
w = w(:);
Mph  = 1;
Mdg  = .1;
chl0 = 1;
plot(w,aw(w),'c-')
plot(w,Mph*aph_(w,chl0),'g-')
plot(w,Mdg*adg_(w,w0,Sg),'r-')
plot(w,a(w,Mph,Mdg,chl0,w0,Sg),'k-')
grid on
set(gca,'xlim',[400 700])

num = xlsread('GIOP.xlsx','bbw','A4:F64');
W = num(:,1);
w = min(W):.01:max(W);
Dbbw = num(:,5);
Dbbwf = num(:,6);

% bbw  = fit(W,Dbbw,'cubicspline');
bbw  = fit(W,Dbbw,'SmoothingSpline','SmoothingParam',.001);
bbwf = fit(W,Dbbwf,'cubicspline');

figure, hold on
plot(W,Dbbw,'k.')
plot(w,bbw(w),'g-')
plot(w,bbwf(w),'c-')
% set(gca,'xlim',[400 700])

bbp_ = @(w,w0,Sp)(w0./w).^Sp;
Sp = 1.03373;
% figure, hold on
% plot(w,.01*bbp(w,w0,Sp),'k-')

bb = @(w,Mbp,w0,Sp)bbw(w) + Mbp*bbp_(w,w0,Sp);

Mbp = 0.001;
w = w(:);
figure, hold on
plot(w,bbw(w),'c-')
plot(w,Mbp*bbp_(w,w0,Sp),'k-')
plot(w,bb(w,Mbp,w0,Sp),'b-')
set(gca,'xlim',[400 700])

% spath = [DIR_save,'model_GIOPplusV2.mat'];
% save(spath,'Aphi','Ephi','aw','bbw','aph_','adg_','bbp_','a','bb')

%--- ��������� GIOP � GIOP plus ---
close all
w = [400:700]';
Mph  = .5;
Mdg  = .1;
chl0 = 2;
Sg = 0.02061;
Sp = 1.03373;
rrs_GIOP     = rrs_model_GIOP_global_vectorized(w, [1, Mdg, Mbp, chl0, Sg, Sp], [], [], 'model_GIOPplusV1', []);
rrs_GIOPplus = rrs_model_GIOP_global_vectorized(w, [1, Mdg, Mbp, chl0, Sg, Sp], [], [], 'model_GIOPplusV2', []);
figure, hold on
plot(w,rrs_GIOP,'g-','LineWidth',3)
plot(w,rrs_GIOPplus,'r:','LineWidth',3)
grid on
drrs = sqrt(sum((rrs_GIOP-rrs_GIOPplus).^2)/length(rrs_GIOP));
title(sprintf('drrs = %g (%g%%)',drrs,100*drrs/mean(rrs_GIOP)))

