clear all
close all
clc

p = mfilename('fullpath');
pos = find(p == '/' | p == '\');
DIR_save = p(1:pos(end-1));

toplot_test = 1;

method_aw  = 1;
method_adg = 4;
method_aph = 1;

method_bbw = 3;

aw  = fun_bioopt_aw(method_aw,toplot_test);
adg = fun_bioopt_adg(method_adg);
aph = fun_bioopt_aph(method_aph);

bbw = fun_bioopt_bbw(method_bbw,toplot_test);

if toplot_test
    
    figure, fun_bioopt_plot(aw,'b-')
    
    figure,
    adg_v2 = fun_bioopt_adg(3);
    fun_bioopt_plot(adg,'g-');
    fun_bioopt_plot(adg_v2,'b--');
    
    figure,
    aph_v2 = fun_bioopt_aph(3);
    fun_bioopt_plot(aph,'g-');
    fun_bioopt_plot(aph_v2,'b--');
    
    figure,
    w_lim_test = [400 800];
    fun_bioopt_plot(bbw,'r-',5,w_lim_test);
    fun_bioopt_plot(bbw,'g-',15,w_lim_test);
    fun_bioopt_plot(bbw,'b-',35,w_lim_test);
    
end   



a = @(w)fun_aw(w) + fun_aph(w,coef_aph) + fun_adg(w,coef_adg);


figure, hold on
w = (400:.1:700)';
plot(w,fun_aw(w),'c-')
plot(w,fun_aph(w,coef_aph),'g-')
plot(w,fun_adg(w,coef_adg),'b-')
plot(w,a(w),'k-')
grid on
 
[fun_bbw,coef_bbw,name_bbw] = fun_bioopt_bbw(3,toplot,'g-',1);



bbp_ = @(w,w0,Sp)(w0./w).^Sp;
Sp = 1.03373;
% figure, hold on
% plot(w,.01*bbp(w,w0,Sp),'k-')

bb = @(w,Mbp,w0,Sp)bbw(w) + Mbp*bbp_(w,w0,Sp);

Mbp = 0.001;
w = w(:);
figure, hold on
plot(w,bbw(w),'c-')
plot(w,Mbp*bbp_(w,w0,Sp),'k-')
plot(w,bb(w,Mbp,w0,Sp),'b-')
set(gca,'xlim',[400 700])

% spath = [DIR_save,'model_GIOPplusV2.mat'];
% save(spath,'Aphi','Ephi','aw','bbw','aph_','adg_','bbp_','a','bb')

%--- ��������� GIOP � GIOP plus ---
close all
w = [400:700]';
Mph  = .5;
Mdg  = .1;
chl0 = 2;
Sg = 0.02061;
Sp = 1.03373;
rrs_GIOP     = rrs_model_GIOP_global_vectorized(w, [1, Mdg, Mbp, chl0, Sg, Sp], [], [], 'model_GIOPplusV1', []);
rrs_GIOPplus = rrs_model_GIOP_global_vectorized(w, [1, Mdg, Mbp, chl0, Sg, Sp], [], [], 'model_GIOPplusV2', []);
figure, hold on
plot(w,rrs_GIOP,'g-','LineWidth',3)
plot(w,rrs_GIOPplus,'r:','LineWidth',3)
grid on
drrs = sqrt(sum((rrs_GIOP-rrs_GIOPplus).^2)/length(rrs_GIOP));
title(sprintf('drrs = %g (%g%%)',drrs,100*drrs/mean(rrs_GIOP)))

