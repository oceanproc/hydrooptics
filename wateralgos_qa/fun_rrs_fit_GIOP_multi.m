function [F_multi,f_multi,gof_multi,k_names,k_multi,k_0_multi,k_lim_multi,rrs_below_multi] = fun_rrs_fit_GIOP_multi(W,RRS,k_0_in,k_lim_in,k_fix_in,Wlim_in,dodefault_in)
% ������������ ���������� �������� �������� ����� ���� ������������� ������� GIOP

if iscell(W)
    num_of_spectra  = length(W);
    rrs_below_multi = cell(num_of_spectra,1);
else
    num_of_spectra  = size(RRS,2);
    rrs_below_multi = nan(size(RRS));
end

F_multi   = nan(num_of_spectra,1);
f_multi   = cell(num_of_spectra,1);
gof_multi = f_multi;

k_multi = [];
for i = 1:num_of_spectra
    
    disp(i)
    
    if iscell(W)
        w   = W{i};
        rrs = RRS{i};
    else
        w   = W;
        rrs = RRS(:,i);
    end
    
    [F,f,gof,kstr,k,k_0,k_lim,rrs_below] = fun_rrs_fit_GIOP(w,rrs,k_0_in,k_lim_in,k_fix_in,Wlim_in,dodefault_in);
    if isempty(k_multi)
        k_multi     = nan(length(k),num_of_spectra);
        k_0_multi   = k_multi;
        k_lim_multi = nan(size(k_lim,1),size(k_lim,2),num_of_spectra);
    end
    
    F_multi(i) = F;
    f_multi{i} = f;
    gof_multi{i} = gof;
    k_multi(:,i) = k;
    k_0_multi(:,i) = k_0;
    k_lim_multi(:,:,i) = k_lim;
    
    if iscell(W)
        rrs_below_multi{i} = rrs_below;
    else
        rrs_below_multi(:,i) = rrs_below;
    end
    
end

k_names = fieldnames(kstr);