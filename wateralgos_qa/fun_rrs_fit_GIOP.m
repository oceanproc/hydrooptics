function [F,f,gof,kstr,k,k_0,k_lim,rrs_below] = fun_rrs_fit_GIOP(W,RRS,k_0,k_lim,k_fix,Wlim,dodefault)
% 
% ������������ ������� �������� ����� ���� ������������� ������� GIOP
% function rrs = rrs_model_GIOP_global(w, Mph, Mdg, Mbp, chl0, Sg, Sbp)
%
%--- ���������� �� ���� ---------------------------------------------------
%
% W - ����� �����
% RRS - ������ ������� ����� ����
% k_0 - ��������� ����������� �������������
%       [Mph, Mdg, Mbp, chl0, Sg, Sp]
%       nan - ���������� �� �������������� ��������
%       [] - ��� ���������� �� nan
% k_lim - ��������� �������� �������������
%       [Mph_min,Mph_max; Mph_min,Mph_max; ... � �.�.]
%       nan - ���������� �� �������������� ��������
%       [] - ��� ���������� �� nan
% k_fix - ������ ������������� �������������:
%         0 - ����������� ����������� �������� ������������
%         1 - �������� ������������ ����������� � ����� ���������� �����������
% Wlim - �������� ���� ���� � �������� �������� ���������� �������������
%
%--------------------------------------------------------------------------
%
%--- ���������� �� ����� --------------------------------------------------
% f - ���������������� �������
% gof - �������� �������������
% kstr- ������������ ������������ � ������� string
% k - ������������ ������������� � ������� �������
%--------------------------------------------------------------------------
%
%--- �������� ������ ------------------------------------------------------
% ��. function rrs = rrs_model_GIOP_global(w, Mph, Mdg, Mbp, chl0, Sg, Sp)
%--------------------------------------------------------------------------

k_names = {'Mph', 'Mdg', 'Mbp', 'chl0', 'Sg', 'Sbp'};

if nargin > 2 && isempty(k_0)
    k_0 = [nan, nan, nan, nan, nan, nan];
end
if nargin > 3 && isempty(k_lim)
    k_lim = [nan,nan; nan,nan; nan,nan; nan,nan; nan,nan; nan,nan];
end

% ������ ��������� �����������
if nargin < 3 || sum(isnan(k_0)) > 0 || isempty(k_0)
    
    ok_490 = W >= 485 & W <= 495;
    ok_555 = W >= 550 & W <= 560;
    R = log10(mean(RRS(ok_490))/mean(RRS(ok_555)));
    
    chl0 = 10.0^(0.319 - 2.336 * R + 0.879 * R^2 - 0.135 * R^3) - 0.071;
    Mph = chl0;
    Mdg = 10.^(-1.20 + 0.47*log10(chl0));
    
%     Sg  = 0.018;
    Sg  = 0.02061;
    
    Mbp   = 1*10^-2;
    
    % ����� 1
    Sbp = 1.03373;
    % ����� 2
%     RRS1 = RRS(W==443);
%     RRS2 = RRS(W==550);
%     rrs1 = rrs_above_to_below(RRS1);
%     rrs2 = rrs_above_to_below(RRS2);
%     Sbp = 2.0*(1.0-1.2*exp(-0.9*rrs1/rrs2));
    
    k_0_default = [Mph,Mdg,Mbp,chl0,Sg,Sbp]';
    
    if nargin < 3 || isempty(k_0)
        k_0 = k_0_default;
    else
        ok_nan = isnan(k_0);
        k_0(ok_nan) = k_0_default(ok_nan);
    end
    
end

% ������ ��������� �������
if nargin < 4 || sum(isnan(k_lim(:)))>0 || isempty(k_lim)

    Mph_lim = [0.03 10];
    
    Mdg_lim  = [0 100];
%     Mdg_lim  = 10.^(-1.20 + 0.47*log10(Mph_lim)); % �� ������ ������
    
    Mbp_lim  = [0 100];
    
    chl0_lim = [0.03 10];   % �� ���� ����
    Sg_lim = [0.01, 0.025]; % �� ���� ����
    
    Sbp_lim = [0 5]; % �� ���� ����
    
    k_lim_default = [Mph_lim; Mdg_lim; Mbp_lim; chl0_lim; Sg_lim; Sbp_lim];
    if nargin < 4 || isempty(k_lim)
        k_lim = k_lim_default;
    else
        ok_nan = isnan(k_lim);
        k_lim(ok_nan) = k_lim_default(ok_nan);
    end

end

% ������ ������������� �������������
if nargin < 5 || isempty(k_fix)
    k_fix = zeros(1,6);
end

if nargin < 6 || length(Wlim) ~= 2
    Wlim = [400 700];
end

if nargin < 7 || isempty(dodefault)
    dodefault = 0;
end

ok_fix = k_fix == 1;
ok_W = W >= Wlim(1) & W <= Wlim(2);

fo = fitoptions('method','NonlinearLeastSquares',...
    'Lower',k_lim(~ok_fix,1),...
    'Upper',k_lim(~ok_fix,2),...
    'StartPoint',k_0(~ok_fix));

if dodefault
    %[F,f,gof,kstr,k,k_0,k_lim,rrs]
    F = nan;
    f = [];
    gof = [];
    kstr = [];
    k = [];
    rrs = rrs_above_to_below(RRS);
    return
%     fo.MaxIter = 2;
end
    
rrs = rrs_above_to_below(RRS);
if sum(ok_fix)>0
    ft = fittype('fun_rrs_model_GIOP_global(w,Mph,Mdg,Mbp,chl0,Sg,Sbp)',...
        'independent','w','coefficients',k_names(~ok_fix),'problem',k_names(ok_fix),...
        'options',fo);
    [f,gof] = fit(W(ok_W),rrs(ok_W),ft,'problem',num2cell(k_0(ok_fix)));
else
    ft = fittype('fun_rrs_model_GIOP_global(w,Mph,Mdg,Mbp,chl0,Sg,Sbp)',...
        'independent','w','coefficients',k_names(~ok_fix),...
        'options',fo);
    [f,gof] = fit(W(ok_W),rrs(ok_W),ft);
end

k = k_0;
k(~ok_fix)=coeffvalues(f);
for i = 1:length(k_names)
    kstr.(k_names{i}) = k(i);
end

F = 100*sum((rrs(ok_W)-f(W(ok_W))).^2/std(rrs(ok_W)));
rrs_below = rrs;

k = k(:);
k_0 = k_0(:);

function rrs = rrs_above_to_below(RRS)
    rrs = RRS./(0.52 + 1.7*RRS);
