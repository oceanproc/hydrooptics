function rrs = fun_rrs_model_GIOP_global(w, Mph, Mdg, Mbp, chl0, Sg, Sp,toplot,w_lim)

if nargin < 8
    toplot = 0;
end

% Werdell et al. (2013) Generalized ocean color inversion model for
%  retrieving marine inherent optical properties, Applied Optics, 52,
%  21 2019-2037.
%
% https://oceancolor.gsfc.nasa.gov/atbd/giop/
% https://oceancolor.gsfc.nasa.gov/meetings/iop_algorithm_workshop/ooxx/franz_and_werdell_2010_ooxx.pdf

% ��������� ������
% https://oceancolor.gsfc.nasa.gov/atbd/giop/giop_l2gen_help.png
%
% ��������� 2010
% https://oceancolor.gsfc.nasa.gov/meetings/iop_algorithm_workshop/ooxx/giop/baseline/
%
% rrs - ����������� ������� ��� ������������ ����
% RRS - ����������� �������, ���������� ��� ������������
% rrs = RRS/(0.52 + 1.7*RRS)
% rrs = rrs_above_to_below(RRS)

%--- ����� ��� ������ ---
% rrs ~ (bb/(a+bb) = u)
% bb - ����� ����������� ��������� ��������� �����
% � - ����� ����������� ���������� �����

% ������� 1 (�� ���������), Gordon et al.(1988)
% rrs = 0.0949*u + 0.0794*u^2

% ������� 2, Ca-������ Morel et al. (2002)
% rrs = G*u, ���
% G - �����. ������������������, ��������� �� ������� ���������, ���������
% ������� �����������, ����������� ��������� �����
%------

%--- ����� ����������� ���������� ---
% a = aw + aph + adg
%
% aw - ����������� ���������� ������ �����
% Pope and Fry (1997)
% https://www.osapublishing.org/ao/abstract.cfm?uri=ao-36-33-8710
%--------

%--- aph - ����������� ���������� ������������ ��������� ---
% aph = sum(aphi)
% aphi = Mphi*aphi_
% Mphi - �������� ���������� i-�� ����������
% aphi_ - ������������ ����� ���������� i-�� ����������

% ����� 1 (�� ���������), Bricaud et al. (1998)
% ������ - http://onlinelibrary.wiley.com/doi/10.1029/98JC02712/epdf
% ��� - https://oceancolor.gsfc.nasa.gov/docs/ocssw/giop_8c_source.html (search bricaud)
% ������ - http://genius.ucsd.edu/Public/ParkRuddick/aph_bricaud_1998.txt
% aphstar(L) = Aphi(L)*chl^(Ephi(L)-1), Aphi, Ephi - ������ Bricaud 1998
% aph_norm = 0.055/aphstar(443,chl)
% aphi_ = aph_norm*aphstar(L,chl)
% chl - ����������� ������ ����������-� (�� ���������� ������������� ���������)
% chl_lim = [0.03 10] (� ����)
%--------

%--- adg - ��������� ����������� ���������� �������������� ��������� � ���
% adg  = sum(adgi)
% adgi = Mdgi*adgi_
% Mdgi - �������� ���������� i-�� ����������
% adgi_ - ������������ ����� ���������� i-�� ����������

% ����� 1 (�� ���������)
% adgi_ = exp(Sdg*(L-L0));
% Sdg - �������������
% Sdg = 0.018 (� ������)
% Sdg = 0.02061, L0 = 443 (� ����);
% Sdg_lim = [.01 .025]; (� ����)
%--------------------------------------------

%--- ����� ����������� ��������� ����� ---
% bb = bbw + bbp
%
% bbw - ����������� ��������� ��������� ����� ������ �����
%
% ����� 1 - �� ���������
% � ����� ������ bbw ������� �� ����������� � ���������
% NASA ���������� ���������� ����������������� sst, � ��������� �� �������
% ��������� ���� ��� ��� ������� �� ����
%
% ����� 2,  Smith and Baker (1981)
% �������������� ��������
% https://www.osapublishing.org/ao/abstract.cfm?uri=ao-20-2-177
%--------

%--- ����������� ��������� ��������� ����� ���������
%
% ����� 1
% bbp = (L0/L)^Sbp
% Sbp = 1.03373 (� ����)
% Sbp_lim = [0 5] (� ����)
% Sbp_min = 0;
% Sbp_max = 5;
% L0 = 443 (� ����)
%
% ����� 2 (�� ��������� - �� ����), , Lee et al 2002
% RRS1 = RRS(W==443);
% RRS2 = RRS(W==550);
% if RRS1 > 0 $ RRS2 > 0
%   rrs1 = rrs_above_to_below(RRS1)
%   rrs2 = rrs_above_to_below(RRS2)
%   Sbp = 2.0*(1.0-1.2*exp(-0.9*rrs1/rrs2));
%   Sbp = min([Sbp,Sbp_max]);
%   Sbp = max([Sbp,Sbp_min]);
% elseif RRS2 > 0
%   Sbp = Sbp_max;
% else
%   fail
% end
%   bbp = (L0/L)^Sbp


% clear all
% close all

% function rrs = rrs_model_GIOP_global(w, Mph, Mdg, Mbp, chl0, Sg, Sp)
% test input
% w = (400:.1:700)';
% Mph = 1;
% Mdg = .1;
% chl0 = 1;
% w0 = 443;
% Sg = 0.018;
% Mbp = 0.001;
% Sp = 1.03;

%     global OAK
OAK = load('model_GIOP');
% OAK = load('model_GIOPplusV2');

% Mph = chl0;

% global OAK
% if nargin < 8
%     clear OAK
%     OAK = load('model_GIOP');
% end

w0 = 443;
a   = OAK.a(w,Mph,Mdg,chl0,w0,Sg);
bb  = OAK.bb(w,Mbp,w0,Sp);

u   = bb./(a + bb);
rrs = 0.0949*u + 0.0794*u.^2;

if toplot
    
    figure,
    
    subplot(2,2,1), hold on
    plot(w,OAK.aw(w),'c-')
    plot(w,Mph*OAK.aph_(w,chl0),'g-')
    plot(w,Mdg*OAK.adg_(w,w0,Sg),'r-')
    plot(w,a,'k-')
    grid on
    set(gca,'xlim',w_lim)
    title('all absorption','FontWeight','normal')
    xlabel('\lambda,nm')
    ylabel('a, m^{-1}')
    legend({'w','ph','dg','tot'},'Location','northwest')
    
    subplot(2,2,2), hold on
    plot(w,OAK.bbw(w),'c-')
    plot(w,Mbp*OAK.bbp_(w,w0,Sp),'b-')
%     plot(w,bb,'b-')
    set(gca,'xlim',w_lim)
    grid on
    title('all. scattering','FontWeight','normal')
    xlabel('\lambda,nm')
    ylabel('bb, m^{-1}')
    legend('w','p')
    
    subplot(2,2,3), hold on
    plot(w,Mph*OAK.aph_(w,chl0),'g-')
    plot(w,Mdg*OAK.adg_(w,w0,Sg),'r-')
    title('aph and adg absorption','FontWeight','normal')
    grid on
    legend('ph','dg')
    ylabel('a,m^{-1}')
    xlabel('\lambda,nm')
    set(gca,'xlim',w_lim)
    
    subplot(2,2,4), hold on
    plot(w,rrs,'k.-')
    grid on
    ylabel('rrs below, sr^{-1}')
    xlabel('\lambda,nm')
    title('modeled rrs','FontWeight','normal')
    set(gca,'xlim',w_lim)
    
end