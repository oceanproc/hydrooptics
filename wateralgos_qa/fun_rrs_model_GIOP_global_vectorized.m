function [rrs,varall] = fun_rrs_model_GIOP_global_vectorized(w, var, varfix, ok_fix, OAK, varnames)
% ����������
% �� ��������� [Mph, Mdg, Mbp, chl0, Sg, Sp]
%
% var - ������ ���������� ����������
% varfix - ������ ������������� ����������
% ok_fix - ���� ������������� ����������
% 
% ������:
% var = [Mph, Mdg, Mbp, Sg, Sp];
% varfix = [chl0];
% ok_fix = [0,0,0,1,0,0];

w = w(:);

% global OAK
if nargin < 3 || isempty(varfix)
    varfix = [];
end
if nargin < 4 || isempty(ok_fix)
    ok_fix = false(size(var));
end
if nargin < 5 || isempty(OAK)
    OAK = load('model_GIOP');
end
if ischar(OAK)
    OAK = load(OAK);
end
if nargin < 6 || isempty(varnames)
    varnames = {'Mph','Mdg','Mbp','chl0','Sg','Sp'};
end
if nargout > 1
    varall = zeros(length(ok_fix),1);
    varall(ok_fix) = varfix;
    varall(~ok_fix) = var;
end

idx_fix    = find(ok_fix);
idx_nonfix = find(~ok_fix);
for i = 1:length(idx_nonfix)
    j = idx_nonfix(i);
    var_struct.(varnames{j}) = var(i);
end
for i = 1:length(idx_fix)
    j = idx_fix(i);
    var_struct.(varnames{j}) = varfix(i);
end

% Werdell et al. (2013) Generalized ocean color inversion model for
%  retrieving marine inherent optical properties, Applied Optics, 52,
%  21 2019-2037.
%
% https://oceancolor.gsfc.nasa.gov/atbd/giop/
% https://oceancolor.gsfc.nasa.gov/meetings/iop_algorithm_workshop/ooxx/franz_and_werdell_2010_ooxx.pdf

% ��������� ������
% https://oceancolor.gsfc.nasa.gov/atbd/giop/giop_l2gen_help.png
%
% ��������� 2010
% https://oceancolor.gsfc.nasa.gov/meetings/iop_algorithm_workshop/ooxx/giop/baseline/
%
% rrs - ����������� ������� ��� ������������ ����
% RRS - ����������� �������, ���������� ��� ������������
% rrs = RRS/(0.52 + 1.7*RRS) 
% rrs = rrs_above_to_below(RRS)

%--- ����� ��� ������ ---
% rrs ~ (bb/(a+bb) = u)
% bb - ����� ����������� ��������� ��������� �����
% � - ����� ����������� ���������� �����

% ������� 1 (�� ���������), Gordon et al.(1988)
% rrs = 0.0949*u + 0.0794*u^2

% ������� 2, Ca-������ Morel et al. (2002)
% rrs = G*u, ���
% G - �����. ������������������, ��������� �� ������� ���������, ���������
% ������� �����������, ����������� ��������� �����
%------

%--- ����� ����������� ���������� ---
% a = aw + aph + adg
% 
% aw - ����������� ���������� ������ �����
% Pope and Fry (1997)
% https://www.osapublishing.org/ao/abstract.cfm?uri=ao-36-33-8710
%--------

%--- aph - ����������� ���������� ������������ ��������� ---
% aph = sum(aphi)
% aphi = Mphi*aphi_
% Mphi - �������� ���������� i-�� ����������
% aphi_ - ������������ ����� ���������� i-�� ����������

% ����� 1 (�� ���������), Bricaud et al. (1998)
% ������ - http://onlinelibrary.wiley.com/doi/10.1029/98JC02712/epdf
% ��� - https://oceancolor.gsfc.nasa.gov/docs/ocssw/giop_8c_source.html (search bricaud)
% ������ - http://genius.ucsd.edu/Public/ParkRuddick/aph_bricaud_1998.txt
% aphstar(L) = Aphi(L)*chl^(Ephi(L)-1), Aphi, Ephi - ������ Bricaud 1998
% aph_norm = 0.055/aphstar(443,chl)
% aphi_ = aph_norm*aphstar(L,chl)
% chl - ����������� ������ ����������-� (�� ���������� ������������� ���������)
% chl_lim = [0.03 10] (� ����)
%--------

%--- adg - ��������� ����������� ���������� �������������� ��������� � ���
% adg  = sum(adgi)
% adgi = Mdgi*adgi_
% Mdgi - �������� ���������� i-�� ����������
% adgi_ - ������������ ����� ���������� i-�� ����������

% ����� 1 (�� ���������)
% adgi_ = exp(Sdg*(L-L0));
% Sdg - �������������
% Sdg = 0.018 (� ������)
% Sdg = 0.02061, L0 = 443 (� ����);
% Sdg_lim = [.01 .025]; (� ����)
%--------------------------------------------

%--- ����� ����������� ��������� ����� ---
% bb = bbw + bbp
% 
% bbw - ����������� ��������� ��������� ����� ������ �����
%
% ����� 1 - �� ���������
% � ����� ������ bbw ������� �� ����������� � ���������
% NASA ���������� ���������� ����������������� sst, � ��������� �� �������
% ��������� ���� ��� ��� ������� �� ����
%
% ����� 2,  Smith and Baker (1981)
% �������������� ��������
% https://www.osapublishing.org/ao/abstract.cfm?uri=ao-20-2-177
%--------

%--- ����������� ��������� ��������� ����� ���������
%
% ����� 1
% bbp = (L0/L)^Sbp
% Sbp = 1.03373 (� ����)
% Sbp_lim = [0 5] (� ����)
% Sbp_min = 0;
% Sbp_max = 5;
% L0 = 443 (� ����)
%
% ����� 2 (�� ��������� - �� ����), , Lee et al 2002
% RRS1 = RRS(W==443);
% RRS2 = RRS(W==550);
% if RRS1 > 0 $ RRS2 > 0
%   rrs1 = rrs_above_to_below(RRS1)
%   rrs2 = rrs_above_to_below(RRS2)
%   Sbp = 2.0*(1.0-1.2*exp(-0.9*rrs1/rrs2));
%   Sbp = min([Sbp,Sbp_max]);
%   Sbp = max([Sbp,Sbp_min]);
% elseif RRS2 > 0
%   Sbp = Sbp_max;
% else
%   fail
% end
%   bbp = (L0/L)^Sbp


% clear all
% close all

% function rrs = rrs_model_GIOP_global(w, Mph, Mdg, Mbp, chl0, Sg, Sp)
% test input
% w = (400:.1:700)';
% Mph = 1;
% Mdg = .1;
% chl0 = 1;
% w0 = 443;
% Sg = 0.018;
% Mbp = 0.001;
% Sp = 1.03;

%     global OAK



w0 = 443;
a   = OAK.a(w,var_struct.Mph,var_struct.Mdg,var_struct.chl0,w0,var_struct.Sg);
bb  = OAK.bb(w,var_struct.Mbp,w0,var_struct.Sp);
u   = bb./(a + bb);

rrs = 0.0949*u + 0.0794*u.^2;

% figure,
% 
% subplot(2,2,1), hold on
% plot(w,OAK.aw(w),'c-')
% plot(w,Mph*OAK.aph_(w,chl0),'g-')
% plot(w,Mdg*OAK.adg_(w,w0,Sg),'r-')
% plot(w,a,'k-')
% grid on
% set(gca,'xlim',[400 700])
% 
% subplot(2,2,2), hold on
% plot(w,OAK.bbw(w),'c-')
% plot(w,Mbp*OAK.bbp_(w,w0,Sp),'k-')
% plot(w,bb,'b-')
% set(gca,'xlim',[400 700])
% grid on
% 
% subplot(2,2,3), hold on
% plot(w,u,'k.-')
% grid on
% 
% subplot(2,2,4), hold on
% plot(w,rrs,'k.-')
% grid on